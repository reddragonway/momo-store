#!/bin/bash
set -e
docker login -u $REGISTRY_USER -p $REGISTRY_PASSWORD $REGISTRY_URL
docker-compose pull
docker-compose up -d --remove-orphans
docker image prune -f
