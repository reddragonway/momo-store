# Momo Store aka Пельменная №2

<img width="900" alt="image" src="https://user-images.githubusercontent.com/9394918/167876466-2c530828-d658-4efe-9064-825626cc6db5.png">

## Структура репозитория
В репозитории есть несколько каталогов:
- В папках frontend и backend лежат исходные коды приложения, манифест CI\CD пайплайна и доп.файлы для деплоя на прод-сервер;
- В папке infrastructure лежат terraform-файлы описывающие виртуальный прод-сервер в Yandex.Cloud;
- В корневой директории лежат docker-compose.yml для запуска приложения через Docker Compose, описательный README-файл и CI\CD манифест из которого запускаются дочернии пайплайны
- Три ветки: main (основной код приложения) / dev (для новых разработок) / original (исходный код для выполнения дипломного проекта)

## Ссылки на ресуры
- Репозиторий приложения: [https://gitlab.com/reddragonway/momo-store](https://gitlab.com/reddragonway/momo-store)
- GitLab Container Registry: [https://gitlab.com/reddragonway/momo-store/container_registry](https://gitlab.com/reddragonway/momo-store/container_registry)
- Прод-сервер: [www.momo-store.ga](http://www.momo-store.ga)
- Презентация проекта с дополнительными пояснениями: [https://gitlab.com/reddragonway/momo-store/-/blob/main/momo-store-presentaion.pdf](https://gitlab.com/reddragonway/momo-store/-/blob/main/momo-store-presentaion.pdf)
# Развертывание приложения

## В РУЧНУЮ:
Приложение можно развернуть локально с помощью команд:

## Frontend

```bash
npm install
NODE_ENV=production VUE_APP_API_URL=http://localhost:8081 npm run serve
```

## Backend

```bash
go run ./cmd/api
go test -v ./... 
```
## Через Docker
Также компоненты приложения можно развернуть в среде Docker с помощью команд:

```bash
docker network create -d bridge momo_network
docker run -d --name momo-backend -p 8081:8081 --network=momo_network --restart always --pull always gitlab.praktikum-services.ru:5050/reddragonway/momo-store/momo-backend:latest
docker run -d --name momo-frontend -p 80:80 --network=momo_network --restart always --pull always gitlab.praktikum-services.ru:5050/reddragonway/momo-store/momo-frontend:latest
```

## Через Docker Compose
Разворачивание стенда через Docker Compose:

```bash
git clone https://gitlab.praktikum-services.ru/reddragonway/momo-store.git
cd ./momo-store
docker-compose pull
docker-compose up -d
```

## Развертывание инфраструктуры
Развертывание инфраструктуры происходит с использованием подхода IaC. В отдельной директории лежат файлы Terraform, с помощью которых можно развернуть готовый стенд в Yandex.Cloud для деплоя приложения.

## Правила внесения изменений в инфраструктуру
- создать новую ветку от dev-ветки для внесения изменений;
- в директории infrastructure изменить компоненты Terraform;
- в ручную запустить terraform apply для внесения изменений в текущую инфраструктуру с любого настроенного для работы с Yandex.Cloud и Terraform сервера
- после проверки результата сделать Merge Request в dev-ветку.

## Релизный цикл приложения
- Планирование и анализ требований
- Определение и утверждение требований
- Проектирование 
- Разработка нового функционала // Развертывание или модернизация инфраструктуры // Написание тестов 
- Тестирование продукта 
- Развертывание, мониторинг и поддержка

## Правила версионирования
Для приложения применяется стандартное версионирования Gitlab кода для коммитов и конвееров. В остальном версионирование идет с упором на манифест SemVer 2.0 Первая версия релиза приложения начинается с тэга 1.0.0. При добавлении новой функциональности меняется минорная цифра версии. При добавлении исправлений в продукт меняется патч-версия. Она берется из номер CI\CD конвеера сборки релиза.
