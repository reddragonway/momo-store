variable "token" {
  description = "yandex access token"
}

terraform {
 required_providers {
  yandex = {
  source  = "yandex-cloud/yandex"
  version = "0.61.0"
  }
 }
}

resource "yandex_compute_instance" "vm-1" {
    name = "momo-store-rdw"

    resources {
        cores  = 2
        memory = 2
    }

    boot_disk {
        initialize_params {
            image_id = "fd8fqpjnaoh5tnfllgu4"
        }
    }

    network_interface {
        subnet_id = "e9b7eqoqn9m285m1tm5s"
        nat       = true
    }

    metadata = {
        user-data = "${file("./meta.txt")}"
    }
} 

output "ip_address" {
    value = yandex_compute_instance.vm-1.network_interface.0.ip_address
}

output "external_ip_address" {
    value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}
